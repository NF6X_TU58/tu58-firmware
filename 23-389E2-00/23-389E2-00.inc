; ---------------------------------------------------------------------------

; Buffer for incoming/outgoing RSP packets
RSP_PKT_BUF_type struc ; (sizeof=0x84)  ; XREF: RAM:RSP_PKT_BUF/r
FLAG:           .db ?                   ; XREF: sub_552-49C/r
                                        ; ROM:01A7/w ... ; CMD/END: 0x02, DATA: 0x01
MBC:            .db ?                   ; XREF: ROM:01C8/w
                                        ; sub_552+9C/r ... ; CMD/END: Message Byte Count=0x0A, DATA: Byte Count
OP_DATA:        .db ?                   ; XREF: sub_552-494/r
                                        ; sub_552-48A/r ... ; CMD/END: Opcode, DATA: Data byte 1
MOD_SUCC:       .db ?                   ; XREF: sub_552+10/r
                                        ; sub_552+15A/r ... ; CMD: Modifier, END: Success Code, DATA: Data byte 2
UNIT:           .db ?                   ; XREF: sub_F0+2A/w
                                        ; sub_2A4:loc_2C8/r ... ; CMD/END: Drive unit, DATA: Data byte 3
SWITCHES:       .db ?                   ; XREF: sub_45+1/w sub_57+17/r ... ; CMD: Switches, END: Not Used, DATA: Data byte 4
SEQ:            .dw ?                   ; CMD/END: Sequence number (always 0), DATA: Data bytes 5-6
ABC:            .dw ?                   ; XREF: sub_552:loc_6FA/r
                                        ; sub_552+212/w ... ; CMD: Byte Count, END: Actual Byte Count, DATA: Data bytes 7-8
BLK_STAT:       .dw ?                   ; XREF: sub_552+1AF/r
                                        ; sub_552+206/w ... ; CMD: Bluck Number, END: Summary Status, DATA: Data bytes 9-10
CHECKSUM:       .dw ?                   ; XREF: sub_552+BB/w
                                        ; sub_552+E0/o ; CMD/END: Checksum, DATA: Data bytes 11-12
DATA13:         .db 118 dup(?)          ; XREF: sub_F0+3C/o ROM:01AD/o ... ; CMD/END: Unused, DATA: Data bytes 13-128 and checksum
RSP_PKT_BUF_type ends

; ---------------------------------------------------------------------------

; enum IO_PORTS
IO_UART:         = 8
IO_8155_CSR:     = 20h
IO_8155_PA:      = 21h                  ; Input port
IO_8155_PB:      = 22h                  ; Output port
IO_8155_PC:      = 23h                  ; Output port
IO_8155_TIMERL:  = 24h
IO_8155_TIMERH:  = 25h

; ---------------------------------------------------------------------------

; 8155 CSR Register Bit Definitions
; enum CSR_BITS, bitfield
PA:              = 1
PB:              = 2
CSR_PC           = 0Ch
PC_ALT1:         = 0
PC_ALT3:         = 4
PC_ALT4:         = 8
PC_ALT2:         = 0Ch
IEA:             = 10h
IEB:             = 20h
CSR_TM           = 0C0h
TM_NOP:          = 0
TM_STOP:         = 40h
TM_STOP_TC:      = 80h
TM_START:        = 0C0h

; ---------------------------------------------------------------------------

; 8155 Input Port A Bit Definitions
; enum PA_BITS, bitfield
WRT_PERMIT_L:    = 1
VEL_TP_H:        = 2
B_CART_L:        = 4
A_CART_L:        = 8
RUN_TP_L:        = 10h
OE:              = 20h
SW_BOOT_L:       = 40h
MARK_H:          = 80h

; ---------------------------------------------------------------------------

; 8155 Output Port B Bit Definitions
; enum PB_BITS, bitfield
RUN_L:           = 1
WRT_ENB_H:       = 2
REVERSE_H:       = 4
SEL_DRV_B_L:     = 8
SEL_TRK_0_L:     = 10h
GAIN_REDUCE_L:   = 20h
ERASE_ENA_L:     = 40h
IPS30_L:         = 80h

; ---------------------------------------------------------------------------

; 8155 Output Port C Bit Definitions (ALT 2 Mode)
; enum PC_BITS, bitfield
BOOT_L:          = 1
TP3:             = 2
TP1:             = 4
LED:             = 8
TP2:             = 10h
PC_BITS5:        = 20h                  ; Unused

; ---------------------------------------------------------------------------

; enum RSP_OPCODES
RSP_OP_NOP:      = 0
RSP_OP_INIT:     = 1
RSP_OP_READ:     = 2
RSP_OP_WRITE:    = 3
RSP_OP_POSITION: = 5
RSP_OP_DIAGNOSE: = 7
RSP_OP_GETSTAT:  = 0Ah
RSP_OP_END:      = 40h

